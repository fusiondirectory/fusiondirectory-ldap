# FusionDirectory LDAP library

Modern Object Oriented PHP LDAP library which is used by FusionDirectory cli tools.

Usage:

```php
<?php

require 'FusionDirectory/Ldap/autoload.php';

use FusionDirectory\Ldap;

$ldap = new Ldap\Link('ldapi:///');
/* Bind as EXTERNAL */
$ldap->saslBind('', '', 'EXTERNAL');

/* Make a search */
$list = $ldap->search('ou=people,dc=example,dc=com', '(cn=*)', ['cn'], 'one');

/* Throw FusionDirectory\Ldap\Exception if there was an error */
$list->assert();

/* Browse results, Ldap\Result is Traversable */
foreach ($list as $dn => $attributes) {
    echo $dn.': '.$attributes['cn'][0]."\n";
}

/* Throw FusionDirectory\Ldap\Exception if there was an error while iterating */
$list->assertIterationWentFine();

/* Ldap\Result is also Countable */
echo 'There was '.count($list).' results'."\n";
```
